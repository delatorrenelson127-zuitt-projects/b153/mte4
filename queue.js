let collection = [];

// Write the queue functions below.
function enqueue(str) {
  collection[collection.length] = str;
  return collection;
}

function print() {
  return collection;
}

function dequeue() {
  if (!isEmpty()) {
    collection[0] = ""; // remove the first element
    for (let i = 0; i < collection.length; i++) {
      collection[i] = collection[i + 1]; // then move element to front
      if (collection[i] === undefined) { // there's and undefined element at the end
        collection.length = collection.length - 1; // remove it
      }
    }
  }
  return collection;
}

function front() {
  return collection[0];
}

function size() {
  return collection.length;
}

function isEmpty() {
  return collection.length === 0 ? true : false;
}

module.exports = { collection, enqueue, print, dequeue, size, front, isEmpty };